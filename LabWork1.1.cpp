﻿float sumOfLargerTwoFromFour(float a, float b, float c, float d)
{
	float firstMax, secondMax;

	firstMax = { a };

	if (firstMax > b) {
		secondMax = { b };
	}
	else {
		firstMax = { b };
		secondMax = { a };
	}
	if (firstMax > c) {
		if (secondMax < c) {
			secondMax = { c };
		}
	}
	else {
		secondMax = { firstMax };
		firstMax = { c };
	}
	if (firstMax > d) {
		if (secondMax < d) {
			secondMax = { d };
		}
	}
	else {
		secondMax = firstMax;
		firstMax = { d };
	}
	
	return firstMax + secondMax;
}


