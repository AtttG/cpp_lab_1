#include <iostream>
#include <fstream>
#include <string> 
#include "LabWork1.1.h"
#include "LabWork1.2.h"
#include "LabWork1.3.h"


using namespace std;

int main()
{


	// ������������ ������ 1.1

	//cout << sumOfLargerTwoFromFour(-2.3, 2, 1.1, 0) << endl;

	// ������������ ������ 1.2

	//cout << solveFunction(2, 3, 1) << endl;

	// ������������ ������ 1.3

	// ������ ����� �� �������� �������
	string line;
	ifstream inStream("lab1.3_input.txt");
	
	int size{};
	int outSize {};
	int* baseArray{};
	char seperator = ',';

	if (inStream.is_open())
	{
		// ��������� ������� �������� ������� �� 1 ������
		getline(inStream, line);
		size = (stoi(line));
		baseArray = new int[size];

		// ��������� �������� ������� �� 2 ������
		getline(inStream, line);
		string value = "";
		int indexCounter{0};

		for (char i: line) {
			if (i != seperator) {
				value += i;
			}
			else {
				baseArray[indexCounter] = stoi(value);
				value = "";
				indexCounter++;
			}
		}
		if (value != "") {
			baseArray[indexCounter] = stoi(value);
		}
		inStream.close();


		int* result = combineRemaindersIntoArray(baseArray, size, outSize);

		// ������ ���������� � ����
		ofstream outStream;
		outStream.open("lab1.3_output.txt");
		if (outStream.is_open()) {

			outStream << outSize << std::endl;

			for (int i = 0; i < outSize; i++) {
				outStream << result[i];

				if (i != (outSize - 1)) { outStream << seperator; }
			}

			outStream.close();

		}
		else {
			cout << "[ERROR] Cant open output file";
		}
	}
	else {
		cout << "[ERROR] Cant open input file";
	}
}