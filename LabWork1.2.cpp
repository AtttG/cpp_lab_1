
// (ax + b)((a - 1) * x + b - 1) ... ((a - k) * x + b - k)) ... (x + 1)

long double solveFunction(long double a, long double b, long double x)
{
	int k{0};

	while (true) {

		k++;

		long double result = { a * x + b };

		long double stepResult{};

		for (int i = 1; i <= k; i++) {

			long double subStep1 = (a - i) < 1 ? 1 : (a - i);
			long double subStep2 = (b - i) < 1 ? 1 : (b - i);

			long double stepResult = subStep1 * x + subStep2;
			
			result *= stepResult;

			if (stepResult == (x + 1)) {
				return result;
			}
		}
	}
}