#include <iostream>


bool isInArray(int* inputArray, int size, int value)
{

	for (int i = 0; i < size; i++) {

		if (inputArray[i] == value) {

			return true;
		}
	}

	return false;

}

int* combineRemaindersIntoArray(int* inputArray, int size, int &outSize) {

	int* deviders = new int [3] {2, 3, 5};

	int uniqueCount{0};

	int* maxPossibleArray = new int[size * 3];

	int reminder{};

	for (int i = 0; i < size; i++) {

		for (int d = 0; d < 3; d++) {

			reminder = { inputArray[i] % deviders[d] };

			if (!isInArray(maxPossibleArray, uniqueCount, reminder)) {
				maxPossibleArray[uniqueCount] = reminder;
				uniqueCount++;
			}


		}
	}
	
	outSize = uniqueCount;

	int* outArray = new int[outSize];

	for (int i = 0; i < outSize; i++) {
		outArray[i] = maxPossibleArray[i];
	}

	return outArray;

}